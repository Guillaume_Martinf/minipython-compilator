package mini_python;

import java.util.Arrays;
import java.util.List;

public enum CompileBinop {
    Badd {
        @Override
        void compileInt(X86_64 asm) {
            asm.movq("8(%rax)", "%rdi");
            asm.movq("8(%rbx)", "%rsi");
            asm.addq("%rsi", "%rdi");
            asm.call("int_to_pointer");
        }

        @Override
        void compileString(X86_64 asm) {
            // Move value to callee saved registers.
            asm.movq("%rax", "%r12");
            asm.movq("%rbx", "%r13");
            // %rdi -> 17 + s1.length + s2.length
            asm.movq("8(%rax)", "%rdi");
            asm.movq("8(%rbx)", "%rsi");
            asm.addq("%rsi", "%rdi");
            asm.addq("%rdi", "%r15");
            asm.addq("$17", "%rdi");
            asm.call("my_malloc");
            asm.movq("%rax", "%r14");
            asm.movq("$3", "(%r14)");
            asm.movq("%r15", "8(%r14)");
            // strcopy(res,str1)
            asm.movq("%r14", "%rdi");
            asm.addq("$16", "%rdi");
            asm.movq("%r12", "%rsi");
            asm.addq("$16", "%rsi");
            asm.call("my_strcpy");
            // strcat(res,str2)
            asm.movq("%r14", "%rdi");
            asm.addq("$16", "%rdi");
            asm.movq("%r13", "%rsi");
            asm.addq("$16", "%rsi");
            asm.call("my_strcat");
            asm.movq("%r14", "%rax");
        }

        @Override
        void compileList(X86_64 asm) {
            // Move value to callee saved registers.
            asm.movq("%rax", "%r12");
            asm.movq("%rbx", "%r13");
            // %rdi -> 16 + s1.length + s2.length
            asm.movq("8(%rax)", "%rdi");
            asm.movq("8(%rbx)", "%rsi");
            asm.addq("%rsi", "%rdi");
            asm.movq("%rdi", "%r14");
            //TODO use leaq
            asm.imulq("$8", "%rdi");
            asm.addq("$16", "%rdi");
            asm.call("my_malloc");
            asm.movq("$4", "(%rax)");
            asm.movq("%r14", "8(%rax)");
            // Save the pointer to the list in %r14
            asm.movq("%rax", "%r14");
            // Moving pointer to the res in %r15
            asm.movq("%r14", "%r15");
            asm.addq("$16", "%r15");
            // Moving pointer to the list to copy in %rbx
            asm.movq("%r12", "%rbx");
            asm.addq("$16", "%rbx");
            asm.movq("8(%r12)", "%rdi");
            Compile.compileLoop(asm, () -> {
                asm.movq("(%rbx)", "%rdi");
                asm.movq("%rdi", "(%r15)");
                asm.addq("$8", "%r15");
                asm.addq("$8", "%rbx");
            });
            asm.movq("%r13", "%rbx");
            asm.addq("$16", "%rbx");
            asm.movq("8(%r13)", "%rdi");
            Compile.compileLoop(asm, () -> {
                asm.movq("(%rbx)", "%rdi");
                asm.movq("%rdi", "(%r15)");
                asm.addq("$8", "%r15");
                asm.addq("$8", "%rbx");
            });
            asm.movq("%r14", "%rax");
            asm.xorq("%r14", "%r14");
        }
    },

    Bsub {
        @Override
        void compileInt(X86_64 asm) {
            asm.movq("8(%rax)", "%rdi");
            asm.movq("8(%rbx)", "%rsi");
            asm.subq("%rsi", "%rdi");
            asm.call("int_to_pointer");
        }
    },
    Bmul {
        @Override
        void compileInt(X86_64 asm) {
            asm.movq("8(%rax)", "%rdi");
            asm.movq("8(%rbx)", "%rsi");
            asm.imulq("%rsi", "%rdi");
            asm.call("int_to_pointer");
        }
    }

    ,
    Bdiv {
        @Override
        void compileInt(X86_64 asm) {
            //The dividend is stored in rdx:rax
            //The divisor is the parameter of idivq
            asm.movq("$0","%rdx");
            asm.movq("8(%rax)", "%rax");
            asm.movq("8(%rbx)", "%rsi");
            asm.idivq("%rsi");
            asm.movq("%rax", "%rdi");
            asm.call("int_to_pointer");
        }
    },
    Bmod() {
        @Override
        void compileInt(X86_64 asm) {
            //The dividend is stored in rdx:rax
            //The divisor is the parameter of idivq
            asm.movq("$0","%rdx");
            asm.movq("8(%rax)", "%rax");
            asm.movq("8(%rbx)", "%rsi");
            asm.idivq("%rsi");
            asm.movq("%rdx", "%rdi");
            asm.call("int_to_pointer");
        }
    },
    Beq() {
        @Override
        void compileBool(X86_64 asm) {
            compileInt(asm);
        }

        @Override
        void compileInt(X86_64 asm) {
            asm.pushq("%r10");
            asm.pushq("%r11");
            asm.movq("8(%rax)", "%r10");
            asm.movq("8(%rbx)", "%r11");
            asm.xorq("%rdi", "%rdi");
            asm.cmpq("%r11", "%r10");
            asm.sete("%dil");
            asm.call("bool_to_pointer");
            asm.popq("%r11");
            asm.popq("%r10");
        }

        @Override
        void compileString(X86_64 asm) {
            this.compareString(asm);
            asm.xorq("%rdi", "%rdi");
            asm.cmpq("$0", "%rax");
            asm.setne("%dil");
            asm.call("bool_to_pointer");
        }

        @Override
        void compileList(X86_64 asm) {
            asm.pushq("%r8");
            asm.pushq("%r9");
            asm.movq("%rax", "%r8");
            asm.movq("%rbx", "%r9");
            asm.movq("8(%r8)", "%rdi");
            asm.movq("8(%r9)", "%rsi");
            asm.cmpq("%rdi", "%rsi");
            asm.jne("Beq_list_false");
            asm.addq("$16", "%r8");
            asm.addq("$16", "%r9");
            Compile.compileLoop(asm, () -> {
                asm.movq("(%r8)", "%rdi");
                asm.movq("(%r9)", "%rsi");
                asm.call("bop_Beq");
                asm.movq("8(%rax)", "%rdi");
                asm.cmpq("$0", "%rdi");
                asm.je("Beq_list_false_pop");
                asm.addq("$8", "%r8");
                asm.addq("$8", "%r9");
            });
            asm.movq("$1", "%rdi");
            asm.call("bool_to_pointer");
            asm.jmp("Beq_list_end");
            asm.label("Beq_list_false_pop");
            // Pops the register linked to the loop (check compileLoop)
            // TODO: Meilleure solution ?
            asm.popq("%rdi");
            asm.popq("%rsi");
            asm.label("Beq_list_false");
            asm.xorq("%rdi", "%rdi");
            asm.call("bool_to_pointer");
            asm.label("Beq_list_end");
            asm.popq("%r9");
            asm.popq("%r8");
        }

        @Override
        public void compile(X86_64 asm) {
            asm.label("bop_" + name());
            asm.movq("%rdi", "%rax");
            asm.movq("%rsi", "%rbx");
            asm.movq("(%rax)", "%rcx");
            asm.movq("(%rbx)", "%rdx");
            // Interprets booleans as integers.
            asm.cmpq("$1", "%rcx");
            asm.jne("beq_1");
            asm.movq("$2", "%rcx");
            asm.label("beq_1");
            asm.cmpq("$1", "%rdx");
            asm.jne("beq_2");
            asm.movq("$2", "%rdx");
            asm.label("beq_2");
            // Check if same type
            asm.cmpq("%rcx", "%rdx");
            asm.jne("beq_not_same_type");

            super.compileSameType(asm);
            asm.jmp("beq_end");
            asm.label("beq_not_same_type");
            asm.movq("$0", "%rdi");
            asm.call("bool_to_pointer");
            asm.label("beq_end");
            asm.ret();
        }

    },
    Bneq() {
        @Override
        public void compile(X86_64 asm) {
            asm.label("bop_" + name());
            asm.call("bop_Beq");
            asm.xorq("$1", "8(%rax)");
            asm.ret();
        }
    },
    Blt() {
        @Override
        void compileBool(X86_64 asm) {
            compileInt(asm);
        }

        @Override
        void compileInt(X86_64 asm) {
            asm.pushq("%r10");
            asm.pushq("%r11");
            asm.movq("8(%rax)", "%r10");
            asm.movq("8(%rbx)", "%r11");
            asm.xorq("%rdi", "%rdi");
            asm.cmpq("%r11", "%r10");
            asm.setl("%dil");
            asm.call("bool_to_pointer");
            asm.popq("%r11");

            asm.popq("%r10");
        }

        @Override
        void compileString(X86_64 asm) {
            this.compareString(asm);
            asm.xorq("%rdi", "%rdi");
            asm.cmpq("$0", "%rax");
            asm.setl("%dil");
            asm.call("bool_to_pointer");
        }

        @Override
        void compileList(X86_64 asm) {
            asm.pushq("%r8");
            asm.pushq("%r9");
            asm.pushq("%r10");
            asm.pushq("%r11");
            asm.movq("%rax", "%r8");
            asm.movq("%rbx", "%r9");
            asm.movq("8(%r8)", "%rdi");
            asm.movq("8(%r9)", "%rsi");
            asm.movq("%rdi", "%r10");
            asm.movq("%rsi", "%r11");
            // %rdi=min(%rdi,%rsi)
            asm.cmpq("%rsi", "%rdi");
            asm.jle("Blt_after_min");
            asm.movq("%rsi", "%rdi");
            asm.label("Blt_after_min");
            asm.addq("$16", "%r8");
            asm.addq("$16", "%r9");
            Compile.compileLoop(asm, () -> {
                asm.movq("(%r8)", "%rdi");
                asm.movq("(%r9)", "%rsi");
                asm.call("bop_Beq");
                asm.movq("8(%rax)", "%rdi");
                asm.cmpq("$1", "%rdi");
                asm.je("Blt_el_eq");
                asm.movq("(%r8)", "%rdi");
                asm.movq("(%r9)", "%rsi");
                asm.call("bop_Blt");
                asm.popq("%rdi");
                asm.popq("%rsi");
                asm.jmp("Blt_list_end");
                asm.label("Blt_el_eq");
                asm.addq("$8", "%r8");
                asm.addq("$8", "%r9");
            });
            // All the elements are the same, we just need to check if len(l1)<len(l2)
            asm.xorq("%rax", "%rax");
            asm.xorq("%rdi", "%rdi");
            asm.cmpq("%r11", "%r10");
            asm.setl("%dil");
            asm.call("bool_to_pointer");
            asm.label("Blt_list_end");
            asm.popq("%r11");
            asm.popq("%r10");
            asm.popq("%r9");
            asm.popq("%r8");
        }
    },
    Ble() {
        @Override
        void compileBool(X86_64 asm) {
            compileInt(asm);
        }

        @Override
        void compileInt(X86_64 asm) {
            asm.movq("8(%rax)", "%r10");
            asm.movq("8(%rbx)", "%r11");
            asm.xorq("%rdi", "%rdi");
            asm.cmpq("%r11", "%r10");
            asm.setle("%dil");
            asm.call("bool_to_pointer");
        }

        @Override
        void compileString(X86_64 asm) {
            this.compareString(asm);
            asm.xorq("%rdi", "%rdi");
            asm.cmpq("$0", "%rax");
            asm.setle("%dil");
            asm.call("bool_to_pointer");
        }

        @Override
        void compileList(X86_64 asm) {
            asm.pushq("%r8");
            asm.pushq("%r9");
            asm.pushq("%r10");
            asm.pushq("%r11");
            asm.movq("%rax", "%r8");
            asm.movq("%rbx", "%r9");
            asm.movq("8(%r8)", "%rdi");
            asm.movq("8(%r9)", "%rsi");
            asm.movq("%rdi", "%r10");
            asm.movq("%rsi", "%r11");
            // %rdi=min(%rdi,%rsi)
            asm.cmpq("%rsi", "%rdi");
            asm.jle("Ble_after_min");
            asm.movq("%rsi", "%rdi");
            asm.label("Ble_after_min");
            asm.addq("$16", "%r8");
            asm.addq("$16", "%r9");
            Compile.compileLoop(asm, () -> {
                asm.movq("(%r8)", "%rdi");
                asm.movq("(%r9)", "%rsi");
                asm.call("bop_Beq");
                asm.movq("8(%rax)", "%rdi");
                asm.cmpq("$1", "%rdi");
                asm.je("Ble_el_eq");
                asm.movq("(%r8)", "%rdi");
                asm.movq("(%r9)", "%rsi");
                asm.call("bop_Blt");
                asm.popq("%rdi");
                asm.popq("%rsi");
                asm.jmp("Ble_list_end");
                asm.label("Ble_el_eq");
                asm.addq("$8", "%r8");
                asm.addq("$8", "%r9");
            });
            // All the elements are the same, we just need to check if len(l1)<len(l2)
            asm.xorq("%rax", "%rax");
            asm.xorq("%rdi", "%rdi");
            asm.cmpq("%r11", "%r10");
            asm.setle("%dil");
            asm.call("bool_to_pointer");
            asm.label("Ble_list_end");
            asm.popq("%r11");
            asm.popq("%r10");
            asm.popq("%r9");
            asm.popq("%r8");
        }
    },
    Bgt() {
        @Override
        public void compile(X86_64 asm) {
            asm.label("bop_" + name());
            asm.call("bop_Ble");
            asm.xorq("$1", "8(%rax)");
            asm.ret();
        }
    },
    Bge() {
        @Override
        public void compile(X86_64 asm) {
            asm.label("bop_" + name());
            asm.call("bop_Blt");
            asm.xorq("$1", "8(%rax)");
            asm.ret();
        }
    };

    static List<String> types = Arrays.asList("none", "bool", "int", "string", "list");

    void compileBool(X86_64 asm) {
        asm.jmp("__err__");
    }

    void compileInt(X86_64 asm) {
        asm.jmp("__err__");
    }

    void compileString(X86_64 asm) {
        asm.jmp("__err__");
    }

    void compileList(X86_64 asm) {
        asm.jmp("__err__");
    }

    public void compile(X86_64 asm, int type) {
        switch (type) {
            case 1:
                compileBool(asm);
                break;
            case 2:
                compileInt(asm);
                break;
            case 3:
                compileString(asm);
                break;
            case 4:
                compileList(asm);
                break;
        }
    }

    /**
     * The type of the element is stored in register %r8 when this function is
     * called.
     */
    public void compile(X86_64 asm) {
        asm.label("bop_" + name());
        asm.movq("%rdi", "%rax");
        asm.movq("%rsi", "%rbx");
        asm.movq("(%rax)", "%rcx");
        asm.movq("(%rbx)", "%rdx");

        asm.cmpq("%rcx", "%rdx");
        asm.jne("__err__");
        compileSameType(asm);
    }

    private void compileSameType(X86_64 asm) {
        // switch to execute the code associated to the type of the data (stored in %r8)
        for (int i = 1; i < 5; i++) {
            asm.cmpq("$" + i, "%rcx");
            asm.je(name() + "_" + types.get(i));
        }
        // If the type is None
        asm.jmp("__err__");

        for (int i = 1; i < 5; i++) {
            asm.label(name() + "_" + types.get(i));
            compile(asm, i);
            asm.jmp(name() + "_end");
        }
        asm.label(name() + "_end");
        asm.ret();
    }

    protected void compareString(X86_64 asm) {
        asm.movq("%rax", "%rdi");
        asm.addq("$16", "%rdi");
        asm.movq("%rbx", "%rsi");
        asm.addq("$16", "%rdi");
        asm.call("my_strcmp");
    }
}