package mini_python;

import java.util.function.Consumer;

public enum HelperFunction {
    MyMalloc((asm) -> {
        asm.pushq("%rbp");
        asm.movq("%rsp", "%rbp");
        asm.andq("$-16", "%rsp");
        asm.call("malloc");
        asm.movq("%rbp", "%rsp");
        asm.popq("%rbp");
    }),
    MyPrintf((asm) -> {
        // Set %rax to 0 before calling printf
        asm.xorq("%rax", "%rax");
        asm.pushq("%rbp");
        asm.movq("%rsp", "%rbp");
        asm.andq("$-16", "%rsp");
        asm.call("printf");
        asm.movq("%rbp", "%rsp");

        asm.popq("%rbp");
    }),
    MyStrcpy((asm) -> {
        asm.pushq("%rbp");
        asm.movq("%rsp", "%rbp");
        asm.andq("$-16", "%rsp");
        asm.call("strcpy");
        asm.movq("%rbp", "%rsp");
        asm.popq("%rbp");
    }),
    MyStrcat((asm) -> {
        asm.pushq("%rbp");
        asm.movq("%rsp", "%rbp");
        asm.andq("$-16", "%rsp");
        asm.call("strcat");
        asm.movq("%rbp", "%rsp");
        asm.popq("%rbp");
    }),
    MyStrcmp((asm) -> {
        asm.pushq("%rbp");
        asm.movq("%rsp", "%rbp");
        asm.andq("$-16", "%rsp");
        asm.call("strcmp");
        asm.movq("%rbp", "%rsp");
        asm.popq("%rbp");
    }),
    // Transforms a boolean value stored in rdi into a pointer to the corresponding
    // boolean.
    BoolToPointer((asm) -> {
        asm.cmpq("$0", "%rdi");
        asm.je("false_part");
        asm.movq("$true", "%rax");
        asm.jmp("bool_to_pointer_end");
        asm.label("false_part");
        asm.movq("$false", "%rax");
        asm.label("bool_to_pointer_end");
    }),
    IntToPointer((asm) -> {
        asm.pushq("%rdi");
        asm.movq("$16", "%rdi");
        asm.call("my_malloc");
        asm.popq("%rdi");
        asm.movq("$2", "(%rax)");
        asm.movq("%rdi", "8(%rax)");
    }),
    __Err__((asm) -> {
        asm.pushq("%rbp");
        asm.movq("%rsp", "%rbp");
        asm.movq("$1", "%rax");
        asm.emit("leave");
    }),
    PreprocessData((asm) -> {
        // Computes none
        asm.movq("$16", "%rdi");
        asm.call("my_malloc");
        asm.movq("$0", "(%rax)");
        asm.movq("$0", "8(%rax)");
        asm.movq("%rax", "(none)");

        // Computes true
        asm.movq("$16", "%rdi");
        asm.call("my_malloc");
        asm.movq("$1", "(%rax)");
        asm.movq("$1", "8(%rax)");
        asm.movq("%rax", "(true)");

        // Computes false
        asm.movq("$16", "%rdi");
        asm.call("my_malloc");
        asm.movq("$1", "(%rax)");
        asm.movq("$0", "8(%rax)");
        asm.movq("%rax", "(false)");

    }),

    MyPrint((asm) -> {
        asm.pushq("%r12");

        // We store the type in %rbx
        asm.movq("%rdi", "%r12");
        asm.movq("(%rdi)", "%rbx");
        asm.cmpq("$0", "%rbx");
        asm.jne("print_1");
        asm.movq("$print_none", "%rdi");
        asm.call("my_printf");
        asm.jmp("print_end");

        asm.label("print_1");
        asm.cmpq("$1", "%rbx");
        asm.jne("print_2");
        asm.cmpq("$0", "8(%r12)");
        asm.je("print_bool_false");
        asm.movq("$print_true", "%rdi");
        asm.jmp("print_bool_after");
        asm.label("print_bool_false");
        asm.movq("$print_false", "%rdi");
        asm.label("print_bool_after");
        asm.call("my_printf");
        asm.jmp("print_end");

        asm.label("print_2");
        asm.cmpq("$2", "%rbx");
        asm.jne("print_3");
        asm.movq("$print_int_format", "%rdi");
        asm.movq("8(%r12)", "%rsi");
        asm.call("my_printf");
        asm.jmp("print_end");

        asm.label("print_3");
        asm.cmpq("$3", "%rbx");
        asm.jne("print_4");
        asm.addq("$16", "%r12");
        asm.movq("%r12", "%rdi");
        asm.call("my_printf");
        asm.jmp("print_end");
        asm.label("print_4");
        // List
        asm.movq("$opening_braket", "%rdi");
        asm.call("my_printf");
        // Pointer to the list in %r12
        asm.movq("8(%r12)", "%rdi");
        asm.addq("$16", "%r12");
        Compile.compileLoop(asm, () -> {
            asm.cmpq("$0", "%rax");
            asm.je("print_list_0");
            asm.movq("$separator", "%rdi");
            asm.call("my_printf");
            asm.label("print_list_0");
            asm.movq("(%r12)", "%rdi");
            asm.call("my_print");
            asm.addq("$8", "%r12");
        });
        asm.movq("$closing_braket", "%rdi");
        asm.call("my_printf");
        asm.label("print_end");
        asm.popq("%r12");
    }),
    Uneg((asm) -> {
        asm.movq("%rdi", "%rax");
        asm.cmpq("$2", "(%rax)");
        asm.jne("__err__");
        asm.negq("8(%rax)");
    }),
    Unot((asm) -> {
        asm.movq("%rdi", "%rax");
        asm.movq("8(%rax)", "%rdi");
        asm.notq("%rdi");
        asm.andq("$1", "%rdi");
        asm.jmp("bool_to_pointer");
    });

    private final Consumer<X86_64> function;

    HelperFunction(Consumer<X86_64> function) {
        this.function = function;
    }

    public void compile(X86_64 asm) {
        asm.label(nameToLabel());
        function.accept(asm);
        asm.ret();
    }

    private String nameToLabel() {
        StringBuilder label = new StringBuilder();
        boolean isFirst = true;

        for (char c : this.name().toCharArray()) {
            if (Character.isUpperCase(c)) {
                if (!isFirst) {
                    label.append('_');
                }
                isFirst = false;
                label.append(Character.toLowerCase(c));
            } else {
                label.append(c);
            }
        }

        return label.toString();
    }
}
