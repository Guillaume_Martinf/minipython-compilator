package mini_python;

import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.Map;
import java.util.stream.Collectors;

class Typing implements Visitor {

  static boolean debug = false;
  private TExpr expr = null;
  private TStmt stmt = null;
  private final HashMap<String, Function> functionMap = new HashMap<>();
  private final HashSet<String> alreadyDefinedFunctions = new HashSet<>(Arrays.asList("len", "list", "range"));

  private final HashMap<Class<?>, String> classToStringMap = new HashMap<>();
  private final HashMap<Unop, String> unopToStringMap = new HashMap<>();
  private final HashMap<Binop, String> binopToStringMap = new HashMap<>();

  private final HashMap<String, Integer> debugMap = new HashMap<>();
  private final HashMap<String, String> debugStringMap = new HashMap<>();


  /**
   * This is the trace of all the variables used by a function.
   * We keep track of the location in case we discover at the end that we used an
   * undefined variable.
   */
  private final HashMap<String, Location> usedVars = new HashMap<>();
  private final HashSet<String> definedVars = new HashSet<>();

  public Typing(LinkedList<Def> def) {
    checkFunctionNames(def);
    initializeFunctionParams(def);
    initializeErrorMaps();
    if (debug)
      initializeDebugMaps();
  }

  void checkFunctionNames(LinkedList<Def> def) {
    HashSet<String> funcNames = new HashSet<>(Arrays.asList("len", "list", "range"));
    for (Def d : def) {
      String fName = d.f.id;
      if (funcNames.contains(fName))
        error(d.f.loc, "The function " + fName + " already exists");
      funcNames.add(fName);
    }
  }

  void initializeFunctionParams(LinkedList<Def> def) {
    for (String id : Arrays.asList("len", "range", "list")) {
      LinkedList<Variable> vars = new LinkedList<>();
      vars.add(Variable.mkVariable("x"));
      functionMap.put(id, new Function(id, vars));
    }
    for (Def d : def) {
      LinkedList<Variable> vars = new LinkedList<>();
      HashSet<String> params = new HashSet<>();
      for (Ident param : d.l) {
        if (params.contains(param.id))
          error(d.f.loc, "bad declaration for function " + d.f.id + ": repetitive formal parameter in the definition of " + d.f.id);
        params.add(param.id);
        vars.add(Variable.mkVariable(param.id));
      }
      Function f = new Function(d.f.id, vars);
      functionMap.put(d.f.id, f);
    }
  }

  void initializeErrorMaps(){
    classToStringMap.put(Cnone.class, "'NoneType'");
    classToStringMap.put(Cbool.class, "'bool'");
    classToStringMap.put(Cint.class, "'int'");
    classToStringMap.put(Cstring.class, "'str'");
    classToStringMap.put(Elist.class, "'list'");

    unopToStringMap.put(Unop.Uneg, "unary -");
    unopToStringMap.put(Unop.Unot, "not");

    binopToStringMap.put(Binop.Badd, "+");
    binopToStringMap.put(Binop.Bsub, "-");
    binopToStringMap.put(Binop.Bmul, "*");
    binopToStringMap.put(Binop.Bdiv, "//");
    binopToStringMap.put(Binop.Bmod, "%");
    binopToStringMap.put(Binop.Band, "and");
    binopToStringMap.put(Binop.Bor, "or");
    binopToStringMap.put(Binop.Beq, "==");
    binopToStringMap.put(Binop.Bneq, "!=");
    binopToStringMap.put(Binop.Bge, ">=");
    binopToStringMap.put(Binop.Bgt, ">");
    binopToStringMap.put(Binop.Ble, "<=");
    binopToStringMap.put(Binop.Blt, "<");
  }

  void initializeDebugMaps(){
    debugMap.put("intop", 0);
    debugMap.put("strcat", 0);
    debugMap.put("lscat", 0);
    debugMap.put("eq", 0);
    debugMap.put("comp", 0);
    debugMap.put("boolop", 0);
    debugMap.put("ifopt", 0);

    debugStringMap.put("intop", "\tInteger operation(s): ");
    debugStringMap.put("strcat", "\tString concatenation(s): ");
    debugStringMap.put("lscat", "\tList concatenation(s): ");
    debugStringMap.put("eq", "\tEquality check(s): ");
    debugStringMap.put("comp", "\tComparison(s): ");
    debugStringMap.put("boolop", "\tBoolean operation(s): ");
    debugStringMap.put("ifopt", "\tCondition optimization(s): ");
  }

  void increaseValue(Map<String, Integer> map, String key) {
    if (map.containsKey(key)) {
      map.put(key, map.get(key) + 1);
    }
  }

  TExpr evalExpr(Expr e) {
    assert expr == null; // Check for non reetrance.
    e.accept(this);
    TExpr res = expr;
    expr = null;
    return res;
  }

  TStmt evalFunction(Function f, Stmt s) {
    usedVars.clear();
    definedVars.clear();
    definedVars.addAll(f.params.stream().map(v -> v.name).collect(Collectors.toList()));

    TStmt res = evalStmt(s);
    // We check that all the used vars are defined locally in the function.
    for (String used : usedVars.keySet()) {
      if (!definedVars.contains(used)) {
        error(usedVars.get(used), "undefined variable: " + used + " is used but not defined");
      }
    }
    return res;
  }

  TStmt evalStmt(Stmt s) {
    assert stmt == null; // Check for non reetrance.
    s.accept(this);
    TStmt res = stmt;
    stmt = null;
    return res;
  }

  @Override
  public void visit(Cnone c) {
  }

  @Override
  public void visit(Cbool c) {
  }

  @Override
  public void visit(Cstring c) {
  }

  @Override
  public void visit(Cint c) {
  }

  @Override
  public void visit(Ecst e) {
    expr = new TEcst(e.c);
  }

  @Override
  public void visit(Ebinop e) {
    if ((e.e1 instanceof Ecst || e.e1.isConstantList()) && (e.e2 instanceof Ecst || e.e2.isConstantList())){

      switch (e.op) {

        case Badd:
          if (e.e1 instanceof Ecst && e.e2 instanceof Ecst){
            Ecst cst1 = (Ecst)e.e1;
            Ecst cst2 = (Ecst)e.e2;
            if (cst1.c instanceof Cint && cst2.c instanceof Cint){
              if (debug) increaseValue(debugMap, "intop");
              expr = new TEcst( new Cint( ((Cint)cst1.c).i + ((Cint)cst2.c).i ));
            } else if (cst1.c instanceof Cstring && cst2.c instanceof Cstring){
              if (debug) increaseValue(debugMap, "strcat");
              expr = new TEcst( new Cstring( ((Cstring)cst1.c).s + ((Cstring)cst2.c).s ));
            } else {
              errorInBinop(cst1, cst2, e.op);
            }
          } else if (e.e1 instanceof Elist && e.e2 instanceof Elist) {
            if (debug) increaseValue(debugMap, "lscat");
            Elist list1 = (Elist)e.e1;
            Elist list2 = (Elist)e.e2;
            LinkedList<Expr> l = new LinkedList<>();
            l.addAll(list1.l);
            l.addAll(list2.l);
            Elist list = new Elist(l);
            list.accept(this); // a verifier
          } else {
            errorInBinop(e.e1, e.e2, e.op);
          }
          break;

        case Bsub:
          if (e.e1 instanceof Ecst && e.e2 instanceof Ecst){
            Ecst cst1 = (Ecst)e.e1;
            Ecst cst2 = (Ecst)e.e2;
            if (cst1.c instanceof Cint && cst2.c instanceof Cint){
              if (debug) increaseValue(debugMap, "intop");
              expr = new TEcst( new Cint( ((Cint)cst1.c).i - ((Cint)cst2.c).i ));
            } else {
              errorInBinop(cst1, cst2, e.op);
            }
          } else {
            errorInBinop(e.e1, e.e2, e.op);
          }
          break;

        case Bmul:
          if (e.e1 instanceof Ecst && e.e2 instanceof Ecst){
            Ecst cst1 = (Ecst)e.e1;
            Ecst cst2 = (Ecst)e.e2;
            if (cst1.c instanceof Cint && cst2.c instanceof Cint){
              if (debug) increaseValue(debugMap, "intop");
              expr = new TEcst( new Cint( ((Cint)cst1.c).i * ((Cint)cst2.c).i ));
            } else {
              errorInBinop(cst1, cst2, e.op);
            }
          } else {
            errorInBinop(e.e1, e.e2, e.op);
          }
          break;

        case Bdiv:
          if (e.e1 instanceof Ecst && e.e2 instanceof Ecst){
            Ecst cst1 = (Ecst)e.e1;
            Ecst cst2 = (Ecst)e.e2;
            if (cst1.c instanceof Cint && cst2.c instanceof Cint){
              if (debug) increaseValue(debugMap, "intop");
              expr = new TEcst( new Cint( ((Cint)cst1.c).i / ((Cint)cst2.c).i ));
            } else {
              errorInBinop(cst1, cst2, e.op);
            }
          } else {
            errorInBinop(e.e1, e.e2, e.op);
          }
          break;

        case Bmod:
          if (e.e1 instanceof Ecst && e.e2 instanceof Ecst){
            Ecst cst1 = (Ecst)e.e1;
            Ecst cst2 = (Ecst)e.e2;
            if (cst1.c instanceof Cint && cst2.c instanceof Cint){
              if (debug) increaseValue(debugMap, "intop");
              expr = new TEcst( new Cint( ((Cint)cst1.c).i % ((Cint)cst2.c).i ));
            } else {
              errorInBinop(cst1, cst2, e.op);
            }
          } else {
            errorInBinop(e.e1, e.e2, e.op);
          }
          break;

        case Beq:
          if (debug) increaseValue(debugMap, "eq");
          if (e.e1 instanceof Ecst && e.e2 instanceof Ecst){
            Ecst cst1 = (Ecst)e.e1;
            Ecst cst2 = (Ecst)e.e2;
            expr = new TEcst(new Cbool(cst1.c.equals(cst2.c)));
          } else if (e.e1 instanceof Elist && e.e2 instanceof Elist){
            Elist list1 = (Elist)e.e1;
            Elist list2 = (Elist)e.e2;
            expr = new TEcst(new Cbool( list1.constantElistEqualsConstantElist(list2) ));
          } else {
            expr = new TEcst(new Cbool(false));
          }
          break;

        case Bneq:
          if (debug) increaseValue(debugMap, "eq");
          if (e.e1 instanceof Ecst && e.e2 instanceof Ecst){
            Ecst cst1 = (Ecst)e.e1;
            Ecst cst2 = (Ecst)e.e2;
            expr = new TEcst(new Cbool(!cst1.c.equals(cst2.c)));
          } else if (e.e1 instanceof Elist && e.e2 instanceof Elist){
            Elist list1 = (Elist)e.e1;
            Elist list2 = (Elist)e.e2;
            expr = new TEcst(new Cbool( ! list1.constantElistEqualsConstantElist(list2) ));
          } else {
            expr = new TEcst(new Cbool(true));
          }
          break;

        case Bge:
          if (e.e1 instanceof Ecst && e.e2 instanceof Ecst){
            if (debug) increaseValue(debugMap, "comp");
            Ecst cst1 = (Ecst)e.e1;
            Ecst cst2 = (Ecst)e.e2;
            try {
              long order = cst1.c.compareTo(cst2.c);
              expr = new TEcst( new Cbool( order >= 0 ));
            } catch (UnsupportedOperationException eop) {
              errorInBinop(cst1, cst2, e.op);
            }
          } else if (e.e1 instanceof Elist && e.e2 instanceof Elist){
            if (debug) increaseValue(debugMap, "comp");
            Elist list1 = (Elist)e.e1;
            Elist list2 = (Elist)e.e2;
            long order = list1.constantElistComparedToConstantElist(list2);
            expr = new TEcst(new Cbool( order >= 0 ));
          } else {
            errorInBinop(e.e1, e.e2, e.op);
          }
          break;

        case Bgt:
          if (e.e1 instanceof Ecst && e.e2 instanceof Ecst){
            if (debug) increaseValue(debugMap, "comp");
            Ecst cst1 = (Ecst)e.e1;
            Ecst cst2 = (Ecst)e.e2;
            try {
              long order = cst1.c.compareTo(cst2.c);
              expr = new TEcst( new Cbool( order > 0 ));
            } catch (UnsupportedOperationException eop) {
              errorInBinop(cst1, cst2, e.op);
            }
          } else if (e.e1 instanceof Elist && e.e2 instanceof Elist){
            if (debug) increaseValue(debugMap, "comp");
            Elist list1 = (Elist)e.e1;
            Elist list2 = (Elist)e.e2;
            long order = list1.constantElistComparedToConstantElist(list2);
            expr = new TEcst(new Cbool( order > 0 ));
          } else {
            errorInBinop(e.e1, e.e2, e.op);
          }
          break;

        case Ble:
          if (e.e1 instanceof Ecst && e.e2 instanceof Ecst){
            if (debug) increaseValue(debugMap, "comp");
            Ecst cst1 = (Ecst)e.e1;
            Ecst cst2 = (Ecst)e.e2;
            try {
              long order = cst1.c.compareTo(cst2.c);
              expr = new TEcst( new Cbool( order <= 0 ));
            } catch (UnsupportedOperationException eop) {
              errorInBinop(cst1, cst2, e.op);
            }
          } else if (e.e1 instanceof Elist && e.e2 instanceof Elist){
            if (debug) increaseValue(debugMap, "comp");
            Elist list1 = (Elist)e.e1;
            Elist list2 = (Elist)e.e2;
            long order = list1.constantElistComparedToConstantElist(list2);
            expr = new TEcst(new Cbool( order <= 0 ));
          } else {
            errorInBinop(e.e1, e.e2, e.op);
          }
          break;

        case Blt:
          if (e.e1 instanceof Ecst && e.e2 instanceof Ecst){
            if (debug) increaseValue(debugMap, "comp");
            Ecst cst1 = (Ecst)e.e1;
            Ecst cst2 = (Ecst)e.e2;
            try {
              long order = cst1.c.compareTo(cst2.c);
              expr = new TEcst( new Cbool( order < 0 ));
            } catch (UnsupportedOperationException eop) {
              errorInBinop(cst1, cst2, e.op);
            }
          } else if (e.e1 instanceof Elist && e.e2 instanceof Elist){
            if (debug) increaseValue(debugMap, "comp");
            Elist list1 = (Elist)e.e1;
            Elist list2 = (Elist)e.e2;
            long order = list1.constantElistComparedToConstantElist(list2);
            expr = new TEcst(new Cbool( order < 0 ));
          } else {
            errorInBinop(e.e1, e.e2, e.op);
          }
          break;

        case Band:
          if (debug) increaseValue(debugMap, "boolop");

          if (e.e1 instanceof Ecst){
            Ecst cst1 = (Ecst)e.e1;
            if (e.e2 instanceof Ecst){  // const and const
              Ecst cst2 = (Ecst)e.e2;
              expr = cst1.c.toBool() ? new TEcst(cst2.c) : new TEcst(cst1.c);
            } else {                    // const and list
              Elist list2 = (Elist)e.e2;
              if (cst1.c.toBool())
                list2.accept(this);
              else
                expr = new TEcst(cst1.c);
            }

          } else {                     
            Elist list1 = (Elist)e.e1;
            if (e.e2 instanceof Ecst){  // list and const
              Ecst cst2 = (Ecst)e.e2;
              if (list1.toBool())
                expr = new TEcst(cst2.c);
              else
                list1.accept(this);
            } else {                    // list and list
              Elist list2 = (Elist)e.e2;
              if (list1.toBool())
                list2.accept(this);
              else
                list1.accept(this);
            }
          }
          break;

        case Bor:
          if (debug) increaseValue(debugMap, "boolop");

          if (e.e1 instanceof Ecst){
            Ecst cst1 = (Ecst)e.e1;
            if (e.e2 instanceof Ecst){  // const or const
              Ecst cst2 = (Ecst)e.e2;
              expr = cst1.c.toBool() ? new TEcst(cst1.c) : new TEcst(cst2.c);
            } else {                    // const or list
              Elist list2 = (Elist)e.e2;
              if (cst1.c.toBool())
                expr = new TEcst(cst1.c);
              else
                list2.accept(this);
            }

          } else {                     
            Elist list1 = (Elist)e.e1;
            if (e.e2 instanceof Ecst){  // list or const
              Ecst cst2 = (Ecst)e.e2;
              if (list1.toBool())
                list1.accept(this);
              else
                expr = new TEcst(cst2.c);
            } else {                    // list or list
              Elist list2 = (Elist)e.e2;
              if (list1.toBool())
                list1.accept(this);
              else
                list2.accept(this);
            }
          }
          break;

        default:
          throw new Error("\nillegal binary operation on constants");
      }

    } else {
      expr = new TEbinop(e.op, evalExpr(e.e1), evalExpr(e.e2));
    }
  }

  @Override
  public void visit(Eunop e) {
    if (e.e instanceof Ecst || e.e instanceof Elist){

      switch (e.op) {
        case Uneg:
          if (e.e instanceof Ecst && ((Ecst)e.e).c instanceof Cint) {
          if (debug) increaseValue(debugMap, "intop");
            expr = new TEcst(new Cint(-((Cint)((Ecst)e.e).c).i));
          } else {
            errorInUnop(e.e, e.op);
          }
          break;

        case Unot:
          if (e.e instanceof Ecst){
            if (debug) increaseValue(debugMap, "boolop");
            Ecst cst = (Ecst)e.e;
            expr = new TEcst(new Cbool( !cst.c.toBool() ));
          } else if (e.e instanceof Elist){
            if (debug) increaseValue(debugMap, "boolop");
            Elist list = (Elist)e.e;
            expr = new TEcst(new Cbool( !list.toBool() ));
          } else {
            errorInUnop(e.e, e.op);
          }
          break;
      }
    } else {
      expr = new TEunop(e.op, evalExpr(e.e));
    }
  }

  @Override
  public void visit(Eident e) {
    if (!usedVars.containsKey(e.x.id))
      usedVars.put(e.x.id, e.x.loc);
    expr = new TEident(Variable.mkVariable(e.x.id));
  }

  void checkArity(Function f, Ecall e) {
    int currentArity = e.l.size();
    if (currentArity != f.params.size())
      error(e.f.loc,"bad arity for function " + f.name + ": called with " + currentArity + " parameter(s) instead of " + f.params.size());
  }

  @Override
  public void visit(Ecall e) {
    // We first check that the called function has been defined before, or is a
    // recursive call.
    if (!alreadyDefinedFunctions.contains(e.f.id))
      error(e.f.loc, "function " + e.f.id + " is called before being defined");

    checkArity(functionMap.get(e.f.id), e);
    if (e.f.id == "list")
      callToRange(e);
    else if (e.f.id == "range")
      error(e.f.loc, "range() can only be called as an argument of list()");
    else if (e.f.id == "len")
      callToLen(e);
    else {
      LinkedList<TExpr> tl = new LinkedList<>();
      for (Expr ex : e.l) {
        tl.add(evalExpr(ex));
      }
      expr = new TEcall(functionMap.get(e.f.id), tl);
    }
  }

  void callToRange(Ecall e) {
    Expr expr = e.l.get(0);
    if (!(expr instanceof Ecall) || ((Ecall) expr).f.id != "range")
      error(e.f.loc, "bad argument: list() can only take range() as an argument");
    Expr innerExpr = ((Ecall) expr).l.get(0);
    this.expr = new TErange(evalExpr(innerExpr));
  }

  void callToLen(Ecall e) {
    expr = new TElen(evalExpr(e.l.get(0)));
  }

  @Override
  public void visit(Eget e) {
    expr = new TEget(evalExpr(e.e1), evalExpr(e.e2));
  }

  @Override
  public void visit(Elist e) {
    LinkedList<TExpr> tl = new LinkedList<>();
    for (Expr ex : e.l) {
      tl.add(evalExpr(ex));
    }
    expr = new TElist(tl);
  }

  @Override
  public void visit(Sif s) {
    TExpr e = evalExpr(s.e);
    TStmt s1 = evalStmt(s.s1);
    TStmt s2 = evalStmt(s.s2);
    if (e instanceof TEcst){
      if (debug) increaseValue(debugMap, "ifopt");
      LinkedList<TStmt> l = new LinkedList<>();
      if (((TEcst)e).c.toBool()){
        l.add(s1);
        stmt = new TSblock(l);
      } else {
        l.add(s2);
        stmt = new TSblock(l);
      }
    } else if (e instanceof TElist){
      if (debug) increaseValue(debugMap, "ifopt");
      LinkedList<TStmt> l = new LinkedList<>();
      if (((TElist)e).l.isEmpty()){
        l.add(s2);
        stmt = new TSblock(l);
      } else {
        l.add(s1);
        stmt = new TSblock(l);
      }
    } else {
      stmt = new TSif(e, s1, s2);
    }
  }

  @Override
  public void visit(Sreturn s) {
    stmt = new TSreturn(evalExpr(s.e));
  }

  @Override
  public void visit(Sassign s) {
    definedVars.add(s.x.id);
    stmt = new TSassign(Variable.mkVariable(s.x.id), evalExpr(s.e));
  }

  @Override
  public void visit(Sprint s) {
    stmt = new TSprint(evalExpr(s.e));
  }

  @Override
  public void visit(Sblock s) {
    LinkedList<TStmt> l = new LinkedList<>();
    for (Stmt innerStmt : s.l) {
      l.add(evalStmt(innerStmt));
    }
    stmt = new TSblock(l);
  }

  @Override
  public void visit(Sfor s) {
    definedVars.add(s.x.id);
    stmt = new TSfor(Variable.mkVariable(s.x.id), evalExpr(s.e), evalStmt(s.s));
  }

  @Override
  public void visit(Seval s) {
    stmt = new TSeval(evalExpr(s.e));
  }

  @Override
  public void visit(Sset s) {
    stmt = new TSset(evalExpr(s.e1), evalExpr(s.e2), evalExpr(s.e3));
  }

  // use this method to signal typing errors
  static void error(Location loc, String msg) {
    throw new Error(loc + "\n" + msg);
  }

  void errorInBinop(Expr e1, Expr e2, Binop op){
    Class<?> class1 = Object.class;
    Class<?> class2 = Object.class;
    Location loc = new Location();

    if (e1 instanceof Elist){
      class1 = Elist.class;
    } else if (e1 instanceof Ecst){
      class1 = ((Ecst)e1).c.getClass();
      loc = ((Ecst)e1).c.loc;
    }

    if (e2 instanceof Elist){
      class2 = Elist.class;
    } else if (e2 instanceof Ecst){
      class2 = ((Ecst)e2).c.getClass();
      loc = loc.isUnknown() ? ((Ecst)e2).c.loc : loc;
    }
    
    String msg = "unsupported operand type(s) for " + binopToStringMap.get(op) + ": " + classToStringMap.get(class1) + " and " + classToStringMap.get(class2);
    if (loc.isUnknown())
      throw new Error(msg);
    else
      error(loc, msg);
  }

  void errorInUnop(Expr e, Unop op){
    Class<?> clss = Object.class;
    Location loc = new Location();

    if (e instanceof Elist){
      clss = Elist.class;
    } else if (e instanceof Ecst){
      clss = ((Ecst)e).c.getClass();
      loc = ((Ecst)e).c.loc;
    }
    String msg = "unsupported operand type for " + unopToStringMap.get(op) + ": " + classToStringMap.get(clss);
    if (loc.isUnknown())
      throw new Error(msg);
    else
      error(loc, msg);
  }

  static TFile file(File f) {
    TFile tFile = new TFile();
    Typing typing = new Typing(f.l);
    for (Def def : f.l) {
      typing.alreadyDefinedFunctions.add(def.f.id);
      Function function = typing.functionMap.get(def.f.id);
      tFile.l.add(new TDef(function, typing.evalFunction(function, def.s)));
    }
    Function main = new Function("main", new LinkedList<>());
    // main is the first function
    tFile.l.addFirst(new TDef(main, typing.evalFunction(main, f.s)));
    // Show debug statistics
    if (debug) {
      System.out.println("Computation done during static typing:");
      for (Map.Entry<String, Integer> entry : typing.debugMap.entrySet()) {
        if (entry.getValue() != 0) {
          System.out.println(typing.debugStringMap.get(entry.getKey()) + entry.getValue());
        }
      }
    }
    return tFile;
  }

}
