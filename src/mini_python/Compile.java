package mini_python;

import java.util.HashMap;
import java.util.Iterator;

class Compile implements TVisitor {
  static int globalCounter = 0;
  static boolean debug = false;
  X86_64 asm = new X86_64();

  HashMap<String, Variable> definedVariables = new HashMap<>();

  int currentOffset = -8;

  /** Add utilitary functions */
  void helperFunctions() {
    // Creates all the builtins linked to binops
    for (CompileBinop binop : CompileBinop.values()) {
      binop.compile(asm);
    }
    for (HelperFunction helperFunction : HelperFunction.values()) {
      helperFunction.compile(asm);
    }
  }

  void data() {
    asm.dlabel("none");
    asm.quad(0);
    asm.quad(0);

    asm.dlabel("true");
    asm.quad(1);
    asm.quad(1);

    asm.dlabel("false");
    asm.quad(1);
    asm.quad(0);

    asm.dlabel("print_int_format");
    asm.string("%d");

    asm.dlabel("print_true");
    asm.string("True");

    asm.dlabel("print_false");
    asm.string("False");

    asm.dlabel("print_none");
    asm.string("None");

    asm.dlabel("escape");
    asm.string("\n");

    asm.dlabel("opening_braket");
    asm.string("[");

    asm.dlabel("closing_braket");
    asm.string("]");

    asm.dlabel("separator");
    asm.string(", ");
  }

  void compile(TDef f) {
    currentOffset = -8;
    int ofs = 16;
    for (Variable var : f.f.params) {
      var.ofs = ofs;
      ofs += 8;
    }
    f.body.accept(this);
    // We want to call recursively the tree and have the corresponding operators

  }

  @Override
  public void visit(Cnone c) {
    asm.movq("$none", "%rax");
  }

  @Override
  public void visit(Cbool c) {
    asm.movq(c.b ? "$true" : "$false", "%rax");
  }

  @Override
  public void visit(Cint c) {

    asm.movq("$" + c.i, "%rdi");
    asm.call("int_to_pointer");
  }

  @Override
  public void visit(Cstring c) {
    int length = c.s.length();
    asm.movq("$" + (24 + length + 1), "%rdi");
    asm.call("my_malloc");
    asm.movq("$3", "(%rax)");
    asm.movq("$" + length, "8(%rax)");
    int i = 0;
    int offst = 0;
    while (i < length){
      if (i == length - 1){
        asm.movq("$" + ((int) c.s.charAt(i)), (16 + offst) + "(%rax)");
        i++;
        offst++;
      } else {
        int first = (int) c.s.charAt(i);
        int second = (int) c.s.charAt(i+1);
        if (first == 92 && second == 110){
          asm.movq("$" + 10, (16 + offst) + "(%rax)");
          i+=2;
          offst++;
        } else if (first == 92 && second == 116){
          asm.movq("$" + 9, (16 + offst) + "(%rax)");
          i+=2;
          offst++;
        } else {
          asm.movq("$" + first, (16 + offst) + "(%rax)");
          i++;
          offst++;
        }
      }
    }
    asm.movq("$0", (16 + offst) + "(%rax)");
  }

  @Override
  public void visit(TEcst e) {
    e.c.accept(this);
  }

  @Override
  public void visit(TEbinop e) {
    int counter = acquireCounter();
    // We treat Band & Bor apart as they need to be lazily executed.
    if (e.op == Binop.Band) {
      e.e1.accept(this);
      asm.movq("8(%rax)", "%r12");
      asm.cmpq("$0", "%r12");
      asm.je("binop_and_false_" + counter);
      e.e2.accept(this);
      asm.movq("8(%rax)", "%rdi");
      asm.jmp("binop_and_end_" + counter);
      asm.label("binop_and_false_" + counter);
      asm.movq("$0", "%rdi");
      asm.label("binop_and_end_" + counter);
      asm.call("bool_to_pointer");
    } else if (e.op == Binop.Bor) {
      e.e1.accept(this);
      asm.movq("8(%rax)", "%r12");
      asm.cmpq("$1", "%r12");
      asm.je("binop_or_true_" + counter);
      e.e2.accept(this);
      asm.movq("8(%rax)", "%rdi");
      asm.jmp("binop_or_end_" + counter);
      asm.label("binop_or_true_" + counter);
      asm.movq("$1", "%rdi");
      asm.label("binop_or_end_" + counter);
      asm.call("bool_to_pointer");
    } else {
      // Computes e1 and store it on the stack
      e.e2.accept(this);
      asm.pushq("%rax");
      // Computes e2 and retrives e1 from the stack
      e.e1.accept(this);
      asm.movq("%rax", "%rdi");
      asm.popq("%rsi");
      asm.call("bop_" + e.op.name());
    }
  }

  @Override
  public void visit(TEunop e) {
    e.e.accept(this);
    asm.movq("%rax", "%rdi");
    asm.call(e.op.name().toLowerCase());
  }

  @Override
  public void visit(TEident e) {
    asm.movq(definedVariables.get(e.x.name).ofs + "(%rbp)", "%rax");
  }

  @Override
  public void visit(TEcall e) {
    // We push all the arguments on the stack.
    int i = 0;
    Iterator<TExpr> iter = e.l.descendingIterator();
    while (iter.hasNext()) {
      TExpr expr = iter.next();
      expr.accept(this);
      asm.pushq("%rax");
      Variable var = e.f.params.get(i);
      var.ofs = 16 + 8 * i;
      definedVariables.put(var.name, var);
      i += 1;
    }
    asm.call(e.f.name);
    for (int j = 0; j < e.l.size(); j++)
      asm.popq("%rdi");
  }

  @Override
  /**
   * e1[e2]
   */
  public void visit(TEget e) {

    e.e2.accept(this);
    // Checks that the offset is an integer and stores it on the stack.
    asm.cmpq("$2", "(%rax)");
    asm.jne("__err__");
    asm.movq("8(%rax)", "%rax");
    asm.imulq("$8", "%rax");
    asm.pushq("%rax");
    e.e1.accept(this);
    // Checks that e1 is a list
    asm.cmpq("$4", "(%rax)");
    asm.jne("__err__");
    asm.popq("%rbx");
    asm.addq("$16", "%rbx");
    asm.addq("%rbx", "%rax");
    asm.movq("(%rax)", "%rax");
  }

  @Override
  public void visit(TElist e) {
    int length = e.l.size();
    asm.movq("$" + (16 + 8 * length), "%rdi");
    asm.call("my_malloc");
    asm.movq("%rax", "%rbx");
    asm.movq("$4", "(%rbx)");
    asm.movq("$" + length, "8(%rbx)");
    int offset = 16;
    for (TExpr expr : e.l) {
      asm.pushq("%rbx");
      expr.accept(this);
      asm.popq("%rbx");
      asm.movq("%rax", offset + "(%rbx)");
      offset += 8;
    }
    asm.movq("%rbx", "%rax");
  }

  @Override
  public void visit(TSif s) {
    int counter = acquireCounter();
    s.e.accept(this);
    asm.cmpq("$0", "8(%rax)");
    asm.je("sif_else_" + counter);
    s.s1.accept(this);
    asm.jmp("sif_end_" + counter);
    asm.label("sif_else_" + counter);
    s.s2.accept(this);
    asm.label("sif_end_" + counter);
  }

  @Override
  public void visit(TSreturn s) {
    s.e.accept(this);
    asm.movq("%rbp", "%rsp");
    asm.popq("%rbp");
    asm.ret();
  }

  @Override
  public void visit(TSassign s) {
    s.e.accept(this);
    if (definedVariables.containsKey(s.x.name)) {
      // s.x is not the same object as the one already defined
      // We retrieve the object using definedVariables and take its ofs.
      asm.movq("%rax", definedVariables.get(s.x.name).ofs + "(%rbp)");
    } else {
      definedVariables.put(s.x.name, s.x);
      s.x.ofs = currentOffset;
      currentOffset -= 8;
      asm.pushq("%rax");
    }
  }

  @Override
  public void visit(TSprint s) {
    s.e.accept(this);
    asm.movq("%rax", "%rdi");
    asm.call("my_print");
    asm.movq("$escape", "%rdi");
    asm.call("my_printf");

  }

  @Override
  public void visit(TSblock s) {
    for (TStmt stmt : s.l) {
      stmt.accept(this);
    }
  }

  @Override
  public void visit(TSfor s) {

    definedVariables.put(s.x.name, s.x);
    s.x.ofs = currentOffset;
    currentOffset -= 8;
    int counter = acquireCounter();
    // Instanciation of x.
    asm.pushq("%rax");
    // Computation of the list
    s.e.accept(this);
    asm.xorq("%r12", "%r12");// TODO: To remove
    asm.cmpq("$4", "(%rax)");
    asm.jne("__err__");
    // %rbx-> pointer to the tail of the list
    asm.movq("8(%rax)", "%rbx");
    asm.imulq("$8", "%rbx");
    asm.addq("$16", "%rbx");
    asm.addq("%rax", "%rbx");
    // %rax-> pointer to the current pos in the list.
    asm.addq("$8", "%rax");
    asm.label("sfor_start_" + counter);
    asm.addq("$8", "%rax");
    asm.cmpq("%rax", "%rbx");
    asm.jle("sfor_end_" + counter);
    // Execution of the statement
    asm.pushq("%rax");
    asm.pushq("%rbx");
    asm.movq("(%rax)", "%rdi");
    asm.movq("%rdi", s.x.ofs + "(%rbp)");
    s.s.accept(this);
    asm.popq("%rbx");
    asm.popq("%rax");
    asm.jmp("sfor_start_" + counter);
    asm.label("sfor_end_" + counter);
  }

  @Override
  public void visit(TSeval s) {
    s.e.accept(this);
  }

  /**
   * e1[e2]=e3
   */
  @Override
  public void visit(TSset s) {
    s.e3.accept(this);
    asm.pushq("%rax");
    s.e2.accept(this);
    asm.cmpq("$2", "(%rax)");
    asm.jne("__err__");
    asm.movq("8(%rax)", "%rax");
    asm.imulq("$8", "%rax");
    asm.pushq("%rax");
    s.e1.accept(this);
    asm.cmpq("$4", "(%rax)");
    asm.jne("__err__");
    asm.popq("%rbx");
    asm.popq("%rcx");
    asm.addq("$16", "%rbx");
    asm.addq("%rbx", "%rax");
    asm.movq("%rcx", "(%rax)");
  }

  @Override
  public void visit(TElen s) {
    int counter = acquireCounter();
    s.e.accept(this);
    asm.pushq("%rdi");
    asm.movq("(%rax)", "%rdi");
    asm.cmpq("$3", "%rdi");
    asm.je("len_after_type_check_" + counter);
    asm.cmpq("$4", "%rdi");
    asm.jne("__err__");
    asm.label("len_after_type_check_" + counter);
    asm.movq("8(%rax)", "%rdi");
    asm.call("int_to_pointer");
    asm.popq("%rdi");
  }

  @Override
  public void visit(TErange s) {
    s.e.accept(this);
    // check argument type
    asm.cmpq("$2", "(%rax)");
    asm.jne("__err__");
    // Saves length to callee saved register.
    asm.movq("8(%rax)", "%r12");
    // Malloc call
    asm.movq("%r12", "%rdi");
    asm.imulq("$8", "%rdi");
    asm.addq("$16", "%rdi");
    asm.call("my_malloc");
    asm.movq("%rax", "%r14");
    asm.movq("$4", "(%r14)");
    asm.movq("%r12", "8(%r14)");
    asm.movq("%r12", "%rdi");
    compileLoop(asm, () -> {
      asm.movq("%rax", "%r12");
      asm.movq("%rax", "%rdi");
      asm.call("int_to_pointer");
      // %rdi-> pointer to the place where the int should be.
      asm.movq("%r12", "%rdi");
      asm.imulq("$8", "%rdi");
      asm.addq("$16", "%rdi");
      asm.addq("%r14", "%rdi");
      asm.movq("%rax", "(%rdi)");
      asm.movq("%r14", "%rax");
    });
  }

  static X86_64 file(TFile f) {
    Compile compile = new Compile();
    compile.data();

    for (int i = 0; i < f.l.size(); i++) {
      TDef def = f.l.get(i);
      compile.asm.globl("main");
      // Compilation of main
      if (i == 0) {
        compile.asm.label("_start");
        compile.asm.label("main");
        // Do not forget to set %rbp here
        compile.asm.pushq("%rbp");
        compile.asm.movq("%rsp", "%rbp");
        compile.compile(def);
        compile.asm.movq("$0", "%rax");
        compile.asm.emit("leave");
        compile.asm.ret();
      } else {
        compile.asm.label(def.f.name);
        // We save the old rbp and point rbp to where it is located.
        compile.asm.pushq("%rbp");
        compile.asm.movq("%rsp", "%rbp");
        compile.compile(def);
        // We make a return statement at the end of the function if
        compile.asm.emit("leave");
        compile.asm.movq("$none", "%rax");
        compile.asm.ret();
      }
    }
    compile.helperFunctions();
    return compile.asm;

  }

  /**
   * Utility function to create a loop from O to %rdi with the code in inner
   * assembly inside of it.
   * The counter can be accessed through
   */
  public static void compileLoop(X86_64 asm, Runnable innerAssembly) {
    int counter = acquireCounter();
    asm.xorq("%rsi", "%rsi");
    asm.label("start_loop_" + counter);
    asm.cmpq("%rsi", "%rdi");
    asm.jle("end_loop_" + counter);
    asm.movq("%rsi", "%rax");
    asm.pushq("%rsi");
    asm.pushq("%rdi");
    innerAssembly.run();
    asm.popq("%rdi");
    asm.popq("%rsi");
    asm.addq("$1", "%rsi");
    asm.jmp("start_loop_" + counter);
    asm.label("end_loop_" + counter);
  }

  private static int acquireCounter() {
    int res = globalCounter;
    globalCounter += 1;
    return res;
  }

}
