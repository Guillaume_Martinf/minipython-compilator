print(not 42)
print(not 0)
print(not "Hello")
print(not "")
print(not True)
print(not False)
print(not None)
print(not [])
print(not [1, 2])
