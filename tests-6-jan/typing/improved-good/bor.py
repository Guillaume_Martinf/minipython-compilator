print(0 or 42)
print(42 or True)
print(True or 42)
print("" or "Hello")
print("Hello" or "World!")
print(None or True)
print(None or "Hello")
print(None or "")
print(False or None)
print(False or 42)
print([] or [1, 2])
print([1] or True)
print(0 or [])
