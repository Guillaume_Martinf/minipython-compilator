if True:
    print("True constant statement")
else:
    print("False constant statement")

if False and 1:
    print("True constant statement")
else:
    print("False constant statement")

if [False] and 1:
    print("True constant statement")
else:
    print("False constant statement")
