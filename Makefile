all:
	mkdir -p bin
	javac -cp lib/java-cup-11a-runtime.jar -d bin src/mini_python/*.java

test:
	mkdir -p bin
	javac -cp lib/java-cup-11a-runtime.jar -d bin src/mini_python/*.java
	java -cp lib/java-cup-11a-runtime.jar:bin mini_python.Main --debug test.py
	gcc -g -o test test.s  
	./test

clean:
	rm -rf bin test test.s
	cd tests-6-jan && ./test clean && cd ..