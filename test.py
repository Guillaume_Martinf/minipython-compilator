def is_prime(n) :
    for i in list(range(n//2)) :
        if n%(i+2)==0 :
            return False
    return True

def get_primes(n) :
    l=[]
    for i in list(range(n-2)):
        if is_prime(i+2) :
            l=l+[i+2]
    return l
print(get_primes(100))
